package net.kingdomsofarden.andrew2060.heroes.skills;

import java.text.DecimalFormat;
import java.util.Random;
import net.kingdomsofarden.andrew2060.api.APIPlugin;
import net.kingdomsofarden.andrew2060.api.entity.FakeFallingBlockManager;
import net.kingdomsofarden.andrew2060.toolhandler.ToolHandlerPlugin;
import net.kingdomsofarden.andrew2060.toolhandler.potions.PotionEffectManager;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.plugin.PluginManager;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;

public class SkillPyroclasm extends TargettedSkill implements Listener {

    public SkillPyroclasm(Heroes plugin) {
        super(plugin, "Pyroclasm");
        setIdentifiers("skill pyroclasm", "skill pclasm");
        setDescription("Roots a target within $0 blocks for $1 seconds, and summons a pillar of flame upon them after a delay, dealing $2 damage "
                + "to everyone in the area and applying mage-fire");
        setArgumentRange(0,1);
        setTypes(SkillType.SILENCABLE, SkillType.DAMAGING, SkillType.FIRE, SkillType.HARMFUL);
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }


    @Override
    public String getDescription(Hero hero) {
        int range = SkillConfigManager.getUseSetting(hero, this, SkillSetting.MAX_DISTANCE, 18, false);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 3000, false);
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 30, false) 
                + hero.getSkillLevel(this) * SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_INCREASE, 1, false);
        DecimalFormat dF = new DecimalFormat("#0.##");
        return getDescription().replace("$0", range + "").replace("$1", dF.format(duration * 0.001)).replace("$2", dF.format(damage));
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.MAX_DISTANCE.node(), Integer.valueOf(18));
        node.set(SkillSetting.DURATION.node(), Long.valueOf(3000));
        node.set(SkillSetting.DAMAGE.node(), Long.valueOf(30));
        node.set(SkillSetting.DAMAGE_INCREASE.node(), Double.valueOf(1));
        return node;
    }


    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        broadcastExecuteText(hero, target);
        CharacterTemplate cT = plugin.getCharacterManager().getCharacter(target);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 3000, false);
        cT.addEffect(new PyroClasmEffect(this, duration, hero));
        return SkillResult.NORMAL;
    }

    private class PyroClasmEffect extends PeriodicExpirableEffect {

        private Hero applier;

        public PyroClasmEffect(Skill skill, long duration, Hero applier) {
            super(skill, "PyroClasm", 100, duration);
            this.applier = applier;
        }

        @Override
        public void apply(CharacterTemplate cT) {
            PotionEffectManager pMan = ToolHandlerPlugin.instance.getPotionEffectHandler();
            LivingEntity e = cT.getEntity();
            pMan.addPotionEffectStacking(PotionEffectType.JUMP.createEffect((int) (getDuration()/1000D*20), -128), e, false);
            pMan.addPotionEffectStacking(PotionEffectType.SLOW.createEffect((int) (getDuration()/1000D*20), 128), e, false);
        }

        @Override
        public void remove(CharacterTemplate cT) {
            LivingEntity lE = cT.getEntity();
            double damage = SkillConfigManager.getUseSetting(applier, skill, SkillSetting.DAMAGE, 30, false) 
                    + applier.getSkillLevel(skill) * SkillConfigManager.getUseSetting(applier, skill, SkillSetting.DAMAGE_INCREASE, 1, false);
            PluginManager pMan = plugin.getServer().getPluginManager();       
            if (Skill.damageCheck(applier.getPlayer(), lE)) {
                EntityCombustByEntityEvent event = new EntityCombustByEntityEvent(applier.getPlayer(), lE, 5);
                pMan.callEvent(event);
                if (!event.isCancelled()) {
                    if (lE.getFireTicks() < 100) {
                        lE.setFireTicks(100);
                    }
                    addSpellTarget(lE,applier);
                    Skill.damageEntity(lE, applier.getEntity(), damage, DamageCause.MAGIC, false);
                }
            }
            for (Entity e : lE.getNearbyEntities(5, 5, 5)) {
                if (e instanceof LivingEntity && Skill.damageCheck(applier.getPlayer(), (LivingEntity) e)) {
                    EntityCombustByEntityEvent event = new EntityCombustByEntityEvent(applier.getPlayer(), e, 5);
                    pMan.callEvent(event);
                    if (!event.isCancelled()) {
                        if (e.getFireTicks() < 100) {
                            e.setFireTicks(100);
                        }
                        addSpellTarget(e,applier);
                        Skill.damageEntity((LivingEntity) e, applier.getEntity(), damage, DamageCause.MAGIC, false);
                    }
                }
            }
        }

        @Override
        public void tick(CharacterTemplate cT) {
            Location loc = cT.getEntity().getLocation();
            Random rand = new Random();
            FakeFallingBlockManager man = APIPlugin.i().getFakeFallingBlockManager();
            for (int i = 0; i < 50; i++) {
                Vector mod = new Vector(rand.nextDouble()*2 - 1, 0, rand.nextDouble() * 2 - 1);
                @SuppressWarnings("deprecation")
                FallingBlock fB = loc.getWorld().spawnFallingBlock(loc.add(mod), Material.LAVA, (byte)0);
                Vector upVector = new Vector(0, 1, 0).normalize();
                fB.setVelocity(upVector.multiply(1 + (i % 10) * 0.05));
                fB.setDropItem(false);
                man.registerFallingBlock(fB.getUniqueId());
            }    
            super.tick(cT);
        }

        @Override
        public void tickHero(Hero arg0) {
            return;
        }

        @Override
        public void tickMonster(Monster arg0) {
            return;
        }

    }



}
