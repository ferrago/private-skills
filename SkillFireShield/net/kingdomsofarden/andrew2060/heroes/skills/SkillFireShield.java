package net.kingdomsofarden.andrew2060.heroes.skills;

import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.CharacterDamageEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.skill.PassiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;

public class SkillFireShield extends PassiveSkill implements Listener {
        
    public SkillFireShield(Heroes plugin) {
        super(plugin, "FireShield");
        this.setDescription("Fire and lava damage drain from mana instead of dealing damage so long as mana is above 50%");
        this.plugin.getServer().getPluginManager().registerEvents(this, this.plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        return getDescription();
    }
    
    @Override
    public void apply(Hero hero) {
        final Effect effect = new FireShieldEffect(this.plugin, hero, this, getName());
        effect.setPersistent(true);
        hero.addEffect(effect);
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onFireDamage(CharacterDamageEvent event) {
        CharacterTemplate cT = this.plugin.getCharacterManager().getCharacter((LivingEntity) event.getEntity());
        DamageCause cause = event.getCause();
        if (cause.equals(DamageCause.LAVA) || cause.equals(DamageCause.FIRE) || cause.equals(DamageCause.FIRE_TICK)) {
            if (cT.hasEffect("FireShield")) {
                if (((FireShieldEffect)cT.getEffect("FireShield")).tick()) {
                    event.setCancelled(true);
                }
            }
        }
        
    }
    
    public class FireShieldEffect extends Effect {
        
        private Hero hero;

        public FireShieldEffect(Heroes heroes, Hero hero, Skill skill, String effectName) {
            super(heroes, skill, effectName);
            this.hero = hero;
        }
        
        public boolean tick() {
            int remMana = hero.getMana() - 2;
            boolean tickable = remMana > hero.getMana() * 0.5;
            if (tickable) {
                hero.setMana(remMana);
            } else {
                hero.setMana((int) Math.floor(hero.getMana() * 0.5));
            }
            return tickable;
        }
        
    }

}
