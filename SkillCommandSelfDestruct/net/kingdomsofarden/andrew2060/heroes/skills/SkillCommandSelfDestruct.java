package net.kingdomsofarden.andrew2060.heroes.skills;


import java.util.ArrayList;
import java.util.Iterator;

import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import org.bukkit.Location;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

import net.kingdomsofarden.andrew2060.heroes.skills.turretModules.Turret;
import net.kingdomsofarden.andrew2060.heroes.skills.turretModules.TurretEffect;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;

public class SkillCommandSelfDestruct extends ActiveSkill {

	public SkillCommandSelfDestruct(Heroes plugin) {
		super(plugin, "CommandSelfDestruct");
		setDescription("Command: SelfDestruct: Self-destructs all your turrets, dealing $1 damage per turret to all units within range");
		setUsage("/skill commandselfdestruct");
		setArgumentRange(0,0);
		setIdentifiers("skill commandselfdestruct");
	}

	@Override
	public SkillResult use(Hero h, String[] arg1) {
		TurretEffect tE;
		if (!h.hasEffect("TurretEffect")) {
			tE = new TurretEffect(plugin, this);
			h.addEffect(tE);
		} else {
			tE = (TurretEffect)h.getEffect("TurretEffect");
		}
		ArrayList<Turret> turrets = ((SkillTurret)this.plugin.getSkillManager().getSkill("Turret")).getTurrets();
		Iterator<Turret> turretIt = turrets.iterator();
		while (turretIt.hasNext()) {
			Turret next = turretIt.next();
			if (next.getCreator() == h) {
				next.setExpirationTime(System.currentTimeMillis());
				Location loc = next.getLoc();
				loc.getWorld().createExplosion(loc,1.0F);
				next.destroyTurret();
				Arrow a = loc.getWorld().spawnArrow(loc, new Vector(0,0,0), 0.6f, 1.6f);
				for (Entity e : a.getNearbyEntities(next.getRange()*2, next.getRange()*2, next.getRange()*2)) {
					if (!(e instanceof LivingEntity)) {
						continue;
					}
					if (Skill.damageCheck(h.getPlayer(), (LivingEntity) e) && (LivingEntity)e != h.getEntity()) {
                        CharacterTemplate cT = this.plugin.getCharacterManager().getCharacter((LivingEntity)e);
					    if (!cT.hasEffect("SDStack")) {
					        this.addSpellTarget(e, h);
						    Skill.damageEntity((LivingEntity) e, h.getEntity(), 40D, DamageCause.MAGIC);
                            if (cT.hasEffect("SDStackTemp")) {
                                cT.addEffect(new ExpirableEffect(this, this.plugin, "SDStack", 1));
                            } else {
                                cT.addEffect(new ExpirableEffect(this, this.plugin, "SDStackTemp", 1));
                            }
					    }
					}
				}
				a.remove();
			} else {
				continue;
			}
		}
		
		return SkillResult.NORMAL;
	}
	@Override
	public String getDescription(Hero h) {
		return getDescription().replace("$1", h.getLevel() + "");
	}
}
