package net.kingdomsofarden.andrew2060.heroes.skills;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

import net.kingdomsofarden.andrew2060.api.APIPlugin;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityCombustByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.TargettedSkill;

public class SkillFireBomb extends TargettedSkill {

    private final static double FIVE_DEGREES;
    private final static List<Vector> VECTOR_LIST;
    
    static {
        VECTOR_LIST = new LinkedList<Vector>();
        FIVE_DEGREES = Math.PI / 36;
        for (double h = -2.5; h < 0; h += 0.5) { // Vertical orientation
            for (int i = 0; i < 72; i++) {
                double angleXZ = i * FIVE_DEGREES;
                double x = Math.sin(angleXZ);
                double z = Math.cos(angleXZ);
                double y = Math.sin(h);
                VECTOR_LIST.add(new Vector(x,y,z));
            }
        }
    }
    
    
    public SkillFireBomb(Heroes plugin) {
        super(plugin, "FireBomb");
        setDescription("Normal: Triggers a wave of fire around self after $1 seconds, dealing $2 damage and applying magefire. "
                + "Drake: Allows placement of firebomb on other people within 16 blocks");
        setIdentifiers("skill firebomb", "skill fbomb");
        setTypes(SkillType.DAMAGING, SkillType.FIRE, SkillType.SILENCABLE);
    }

    @Override
    public SkillResult use(Hero hero, LivingEntity target, String[] args) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
        if (hero.hasEffect("Drake")) {
            broadcast(hero.getPlayer().getLocation(), "$1 placed a firebomb on himself!", hero.getName());
            hero.addEffect(new FireBombEffect(this, this.plugin, duration, hero));
        } else {
            broadcastExecuteText(hero, target);
            CharacterTemplate cT = plugin.getCharacterManager().getCharacter(target);
            cT.addEffect(new FireBombEffect(this, this.plugin, duration, hero));
        }
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 30, false) 
                + hero.getSkillLevel(this) * SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_INCREASE, 1, false);
        double duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false) * 0.001;
        DecimalFormat dF = new DecimalFormat("#0.##");
        return getDescription().replace("$1", dF.format(duration)).replace("$2", dF.format(damage));
    }
    
    private class FireBombEffect extends ExpirableEffect {

        private Hero hero;
        
        public FireBombEffect(Skill skill, Heroes plugin, long duration, Hero applier) {
            super(skill, plugin, "FireBomb", duration);
            this.hero = applier;
        }
        
        @Override
        public void remove(CharacterTemplate cT) {
            super.remove(cT);
            LivingEntity lE = cT.getEntity();
            double damage = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE, 30, false) 
                    + hero.getSkillLevel(skill) * SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE_INCREASE, 1, false);
            if (!lE.getUniqueId().equals(this.hero.getEntity().getUniqueId())) {
                if (Skill.damageCheck(hero.getPlayer(), lE)) {
                    EntityCombustByEntityEvent event = new EntityCombustByEntityEvent(hero.getPlayer(), lE, 5);
                    plugin.getServer().getPluginManager().callEvent(event);
                    if (!event.isCancelled()) {
                        if (lE.getFireTicks() < 100) {
                            lE.setFireTicks(100);
                        }
                        skill.addSpellTarget(lE, hero);
                        Skill.damageEntity(lE, hero.getPlayer(), damage, DamageCause.MAGIC, false);
                    }
                }
            }
            for (Entity e : lE.getNearbyEntities(8, 5, 8)) {
                if (e instanceof LivingEntity) {
                    EntityCombustByEntityEvent event = new EntityCombustByEntityEvent(hero.getPlayer(), e, 5);
                    plugin.getServer().getPluginManager().callEvent(event);
                    if (!event.isCancelled()) {
                        if (e.getFireTicks() < 100) {
                            e.setFireTicks(100);
                        }
                        skill.addSpellTarget(e, hero);
                        Skill.damageEntity((LivingEntity) e, hero.getPlayer(), damage, DamageCause.MAGIC, false);
                    }
                }
            }
            Location loc = lE.getEyeLocation();
            for (Vector v : VECTOR_LIST) {
                @SuppressWarnings("deprecation")
                Entity e = loc.getWorld().spawnFallingBlock(loc, Material.LAVA, (byte) 0);
                APIPlugin.i().getFakeFallingBlockManager().registerFallingBlock(e.getUniqueId());
                e.setVelocity(v);
            }
            
        }
        
        
    }

}
