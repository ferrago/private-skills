package net.kingdomsofarden.andrew2060.heroes.skills;

import java.text.DecimalFormat;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import pgDev.bukkit.DisguiseCraft.DisguiseCraft;
import pgDev.bukkit.DisguiseCraft.api.DisguiseCraftAPI;
import pgDev.bukkit.DisguiseCraft.disguise.Disguise;
import pgDev.bukkit.DisguiseCraft.disguise.DisguiseType;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.CharacterDamageEvent;
import com.herocraftonline.heroes.api.events.SkillDamageEvent;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;

public class SkillDrake extends ActiveSkill implements Listener {

    public SkillDrake(Heroes plugin) {
        super(plugin, "Drake");
        setIdentifiers("skill drake");
        setUsage("/skill drake");
        setTypes(SkillType.BUFF, SkillType.FIRE, SkillType.SILENCABLE);
        setArgumentRange(0,0);
        setDescription("Transforms the user into a drake for $1 seconds. While active, incoming damage is reduced by $2%, physical attacks deal $3% more damage, and spells gain special effects.");
    }

    @Override
    public SkillResult use(Hero hero, String[] arg1) {
        broadcastExecuteText(hero);
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION.node(), 45000, false);
        hero.addEffect(new DrakeEffect(this, this.plugin, duration));
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        double duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 45000, false) * 0.001;
        double dmgReduc = SkillConfigManager.getUseSetting(hero, this, "defense-bonus", 0.5, false) * 100;
        double dmgBoost = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 0.3, false) * 100;
        DecimalFormat dF = new DecimalFormat("##.##");
        return getDescription().replace("$1",dF.format(duration)).replace("$2",dF.format(dmgReduc)).replace("$3",dF.format(dmgBoost));
    }
    
    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DURATION.node(), 45000);
        node.set("defense-bonus", 0.5);
        node.set(SkillSetting.DAMAGE.node(), 0.3);
        return node;
    }
    
    @EventHandler
    public void onWeaponDamage(WeaponDamageEvent event) {
        if (event.getDamager() instanceof Hero) {
            Hero h = (Hero) event.getDamager();
            if (h.hasEffect("Drake")) {
                double dmgBoost = SkillConfigManager.getUseSetting(h, this, SkillSetting.DAMAGE, 0.3, false);
                event.setDamage(event.getDamage() * (1 + dmgBoost));
            }
        }
        if (event.getEntity() instanceof Player) {
            Hero h = plugin.getCharacterManager().getHero((Player) event.getEntity());
            if (h.hasEffect("Drake")) {
                double dmgReduc = SkillConfigManager.getUseSetting(h, this, "defense-bonus", 0.5, false);
                event.setDamage(event.getDamage() * (1  - dmgReduc));
            }
        }
        
    }
    
    @EventHandler
    public void onSkillDamage(SkillDamageEvent event) {
        if (event.getDamager() instanceof Hero) {
            Hero h = (Hero) event.getDamager();
            if (h.hasEffect("Drake")) {
                double dmgBoost = SkillConfigManager.getUseSetting(h, this, SkillSetting.DAMAGE, 0.3, false);
                event.setDamage(event.getDamage() * (1 + dmgBoost));
            }
        }
        if (event.getEntity() instanceof Player) {
            Hero h = plugin.getCharacterManager().getHero((Player) event.getEntity());
            if (h.hasEffect("Drake")) {
                double dmgReduc = SkillConfigManager.getUseSetting(h, this, "defense-bonus", 0.5, false);
                event.setDamage(event.getDamage() * (1  - dmgReduc));
            }
        }
    }
    
    @EventHandler
    public void onCharacterDamage(CharacterDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            Hero h = plugin.getCharacterManager().getHero((Player) event.getEntity());
            if (h.hasEffect("Drake")) {
                double dmgReduc = SkillConfigManager.getUseSetting(h, this, "defense-bonus", 0.5, false);
                event.setDamage(event.getDamage() * (1  - dmgReduc));
            }
        }
    }
    
    private class DrakeEffect extends ExpirableEffect {

        DisguiseCraftAPI api;
        
        public DrakeEffect(Skill skill, Heroes plugin, long duration) {
            super(skill, plugin, "Drake", duration);
            this.api = DisguiseCraft.getAPI();
        }
        
        @Override
        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            api.disguisePlayer(hero.getPlayer(), new Disguise(api.newEntityID(), DisguiseType.EnderDragon));
        }
        
        @Override
        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            DisguiseCraft.getAPI().undisguisePlayer(hero.getPlayer());
        }
        
    }

}
