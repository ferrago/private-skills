
package net.kingdomsofarden.andrew2060.heroes.skills.aura;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.util.Messaging;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class AuraEffect extends PeriodicEffect {
	public AuraWrapper fWrapper;
	public AuraEffect(Heroes plugin, AuraWrapper funcWrapper) {
		super(plugin, "AuraEffect", 40L);
		fWrapper = funcWrapper;
	}
	
	@Override
	public void applyToHero(Hero h) {
	    h.addEffect(new ExpirableEffect(null, plugin, "AuraChangeCooldown",10000));
		fWrapper.onApply(h);
	}
	
	@Override
	public void tickHero(Hero h) {
		fWrapper.onTick(h);
	}
	@Override
	public void removeFromHero(Hero h) {
		fWrapper.onEnd(h);
	}
    @Override
    public void broadcast(Location source, String message, Object... args) {
        if ((message == null) || message.isEmpty() || message.equalsIgnoreCase("off")) {
            return;
        }

        for (final Player player : plugin.getServer().getOnlinePlayers()) {
            final Location playerLocation = player.getLocation();
            final Hero hero = plugin.getCharacterManager().getHero(player);
            if (source.getWorld().equals(playerLocation.getWorld()) && isInMsgRange(playerLocation, source)) {
                Messaging.send(player, message, args);
            }
        }
    }

    public SkillResult setFWrapper(AuraWrapper wrapper, Hero h) {
        if (!h.hasEffect("AuraChangeCooldown")) {
            this.fWrapper = wrapper;
            broadcast(h.getPlayer().getLocation(), "§7[§2Aura§7] $1 has activated the aura $2",
                    new Object[] {h.getName(), wrapper.auraName});
            this.fWrapper.onApply(h);
            return SkillResult.NORMAL;
        } else {
            h.getPlayer().sendMessage("§7[§2Aura§7] $1 Too soon to change auras again!");
            return SkillResult.INVALID_TARGET_NO_MSG;
        }


    }

    private boolean isInMsgRange(Location loc1, Location loc2) {
        return (Math.abs(loc1.getBlockX() - loc2.getBlockX()) < 25) && (Math.abs(loc1.getBlockY()
                - loc2.getBlockY()) < 25) && (Math.abs(loc1.getBlockZ() - loc2.getBlockZ()) < 25);
    }
}
