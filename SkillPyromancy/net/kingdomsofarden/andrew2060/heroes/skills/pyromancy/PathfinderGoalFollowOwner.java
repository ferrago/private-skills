package net.kingdomsofarden.andrew2060.heroes.skills.pyromancy;

import net.minecraft.server.v1_7_R4.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftLivingEntity;
import org.bukkit.entity.LivingEntity;


public class PathfinderGoalFollowOwner extends PathfinderGoalDeobfuscated {
    private float startDistance;
    private float teleportDistance;
    private float stopDistance;
    private PyromancyBlaze summons;
    private EntityLiving owner;
    private Navigation nav;
    private int timer;
    private boolean map_i;
    private World world;


    public PathfinderGoalFollowOwner(LivingEntity owner, PyromancyBlaze summons) {
        this.owner = ((CraftLivingEntity)owner).getHandle();
        this.summons = summons;
        this.world = summons.world;
        this.nav = summons.getNavigation();
        this.startDistance = 12;
        this.teleportDistance = 30;
        this.stopDistance = 4;
    }

    @Override
    public boolean canStart() {
        if (!this.summons.isAlive()) {
            return false;
        } else if (this.owner == null) {
            return false;
        } else if (this.owner.e(this.owner) < this.startDistance) {
            return false;
        } else return !(this.summons.getGoalTarget() != null && this.summons.getGoalTarget().isAlive());
    }

    @Override
    public boolean canContinue() {
        if (this.nav.g()) {
            return false;
        } else if (this.owner == null) {
            return false;
        } else if (this.summons.e(this.owner) <= this.stopDistance) {
            return false;
        }
        //Goal attackGoal = (PetGoalMeleeAttack) this.dragon.petGoalSelector.getGoal("Attack");
        //return !(attackGoal != null && attackGoal.isActive);
        return true; // TODO: don't pathfind while owner is in combat
    }

    @Override
    public void startGoal() {
        this.timer = 0;

        //Set pathfinding radius
        this.summons.getAttributeInstance(GenericAttributes.b).setValue(this.teleportDistance);
    }

    @Override
    public void finishGoal() {
        this.nav.h();
    }

    @Override
    public void tick() {
        this.summons.getControllerLook().a(this.owner, 10.0F, (float) this.summons.x());
        if (--this.timer <= 0) {
            this.timer = 10;

            double speed = 0.55F;
            if (this.summons.e(this.owner) > (this.teleportDistance) && this.owner.onGround) {
                Location loc = this.owner.getBukkitEntity().getLocation();
                this.summons.teleportTo(loc, false);
                return;
            }

            if (this.summons.getGoalTarget() == null) {
                //Smooth path finding to entity instead of location
                PathEntity path = this.summons.world.findPath(this.summons, owner,
                        (float) this.summons.getAttributeInstance(GenericAttributes.b).getValue(),
                        true, false, false, true);
                this.summons.setPathEntity(path);
                nav.a(path, speed);
            }
        }
    }

}
