package net.kingdomsofarden.andrew2060.heroes.skills.pyromancy;

import net.minecraft.server.v1_8_R1.PathfinderGoal;
/**
 * Convenience class that shows deobfuscated mappings for pathfindergoal logic 
 * 
 * @author Andrew
 *
 */
public abstract class PathfinderGoalDeobfuscated extends PathfinderGoal {

    @Override
    public final boolean a() {
        return canStart();
    }
    
    @Override
    public final boolean b() {
        return canContinue();
    }
    
    @Override
    public final void c() {
        startGoal();
    }
    
    @Override
    public final void d() {
        finishGoal();
    }
    
    @Override
    public final void e() {
        tick();
    }
    
    public abstract boolean canStart();
    public abstract boolean canContinue();
    public abstract void startGoal();
    public abstract void finishGoal();
    public void tick() {}
    
    
}
