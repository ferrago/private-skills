package net.kingdomsofarden.andrew2060.heroes.skills.pyromancy;

import net.minecraft.server.v1_8_R1.EntityLiving;
import net.minecraft.server.v1_8_R1.EntitySmallFireball;
import net.minecraft.server.v1_8_R1.MovingObjectPosition;
import net.minecraft.server.v1_8_R1.World;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityCombustByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.common.CombustEffect;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;

public class PyromancyBlazeFireball extends EntitySmallFireball {

    private Hero hero;
    private Skill skill;

    public PyromancyBlazeFireball(World world, EntityLiving shooter, double x, double y, double z, Hero hero, Skill skill) {
        super(world,shooter,x,y,z);
        this.hero = hero;
        this.skill = skill;
    }

    protected void a(MovingObjectPosition movingobjectposition) {
        try {
            if (!this.world.isStatic) {
                if (movingobjectposition.entity != null) {
                    Entity target = movingobjectposition.entity.getBukkitEntity();
                    if (!(target instanceof LivingEntity)) {
                        return;
                    }
                    if (Skill.damageCheck(hero.getPlayer(), (LivingEntity) target)) {
                        int fireLength = (int) Math.ceil(SkillConfigManager.getUseSetting(hero, skill, "fire-ticks", 40, false)/20);
                        EntityCombustByEntityEvent event = new EntityCombustByEntityEvent(hero.getPlayer(), target, fireLength);
                        Bukkit.getPluginManager().callEvent(event);
                        if (!event.isCancelled()) {
                            target.setFireTicks(event.getDuration() * 20);
                            Heroes.getInstance().getCharacterManager().getCharacter((LivingEntity) target).addEffect(new CombustEffect(skill, (Player) hero.getPlayer()));
                            skill.addSpellTarget(target, hero);
                            double damage = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE, 5, false);
                            damage += (SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE_INCREASE, 0.1, false) * hero.getSkillLevel(skill));
                            Skill.damageEntity((LivingEntity) target, hero.getPlayer(), damage, DamageCause.MAGIC, false);
                        }
                    }
                }

                this.die();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            this.die();
        }
    }
}
