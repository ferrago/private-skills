package net.kingdomsofarden.andrew2060.heroes.skills.pyromancy;

import org.bukkit.entity.LivingEntity;

import net.kingdomsofarden.andrew2060.heroes.skills.SkillPyromancy.PyromancySummonsEffect;

public class PathfinderGoalCheckPyroValidity extends PathfinderGoalDeobfuscated {
    
    private LivingEntity summons;
    private PyromancySummonsEffect effect;

    public PathfinderGoalCheckPyroValidity(LivingEntity summons, PyromancySummonsEffect effect) {
        this.summons = summons;
        this.effect = effect;
    }

    @Override
    public boolean canStart() {
        return !(effect.isValidSummons(summons.getUniqueId()) 
                && effect.getOwner().getPlayer().getWorld().getUID().equals(summons.getWorld().getUID()));
    }

    @Override
    public boolean canContinue() {
        return true;
    }

    @Override
    public void startGoal() {
        return;
    }

    @Override
    public void finishGoal() {
        if (summons.isValid()) {
            summons.remove();
        }
        this.effect = null;
        return;
    }

}
