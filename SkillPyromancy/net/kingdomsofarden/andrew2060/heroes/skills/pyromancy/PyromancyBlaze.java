package net.kingdomsofarden.andrew2060.heroes.skills.pyromancy;

import org.bukkit.craftbukkit.v1_8_R1.entity.CraftLivingEntity;
import org.bukkit.entity.Creature;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.Skill;

import net.kingdomsofarden.andrew2060.heroes.skills.SkillPyromancy;
import net.kingdomsofarden.andrew2060.heroes.skills.SkillPyromancy.PyromancySummonsEffect;
import net.kingdomsofarden.andrew2060.heroes.skills.SkillPyromancy.PyromancyTargetEffect;
import net.minecraft.server.v1_8_R1.Entity;
import net.minecraft.server.v1_8_R1.EntityBlaze;
import net.minecraft.server.v1_8_R1.EntityHuman;
import net.minecraft.server.v1_8_R1.MathHelper;
import net.minecraft.server.v1_8_R1.World;

public class PyromancyBlaze extends EntityBlaze {
    
    private PyromancySummonsEffect linkedEffect;
    private LivingEntity owner;
    private int br;
    private Skill skill;
    
    public PyromancyBlaze(World world) {
        super(world);
        this.die();
    }
    
    public PyromancyBlaze(World world, PyromancySummonsEffect effect, LivingEntity owner, Skill skill) {
        super(world);
        this.skill = skill;
        this.br = 0;
        this.linkedEffect = effect;
        
        
        this.targetSelector.a(0, new PathfinderGoalFindTarget(this));
        this.goalSelector.a(0, new PathfinderGoalCheckPyroValidity(((LivingEntity)this.getBukkitEntity()), linkedEffect));
        this.goalSelector.a(1, new PathfinderGoalFollowOwner(owner, this));
        this.goalSelector.a(2, new PathfinderGoalAttackTarget(this));
    }
    
    @Override
    protected Entity findTarget() {
        if (owner instanceof Player) {
            Hero h = Heroes.getInstance().getCharacterManager().getHero((Player)owner);
            PyromancyTargetEffect effect = SkillPyromancy.getInstance().targetEffects.getUnchecked(h.getPlayer().getUniqueId());
            LivingEntity target = effect.getLastTarget();
            if (target != null) {
                return ((CraftLivingEntity)target).getHandle();
            } else {
                return null;
            }
        } else if (owner instanceof Creature) {
            return ((CraftLivingEntity)((Creature) owner).getTarget()).getHandle();
        } else { //Should not ever be called
            return null;
        }
    }
    
    public void callAttackTick(Entity target, float f) {
        a(target, f);
    }
    
    @Override
    protected void a(Entity entity, float f) {  
        if (this.attackTicks <= 0 && f < 2.0F && entity.boundingBox.e > this.boundingBox.b && entity.boundingBox.b < this.boundingBox.e) {
            this.attackTicks = 20;
            this.n(entity);
        } else if (f < 30.0F) {
            double d0 = entity.locX - this.locX;
            double d1 = entity.boundingBox.b + (double) (entity.length / 2.0F) - (this.locY + (double) (this.length / 2.0F));
            double d2 = entity.locZ - this.locZ;

            if (this.attackTicks == 0) {
                ++this.br;
                if (this.br == 1) {
                    this.attackTicks = 60;
                    this.a(true);
                } else if (this.br <= 4) {
                    this.attackTicks = 6;
                } else {
                    this.attackTicks = 100;
                    this.br = 0;
                    this.a(false);
                }

                if (this.br > 1) {
                    float f1 = MathHelper.c(f) * 0.5F;

                    this.world.a((EntityHuman) null, 1009, (int) this.locX, (int) this.locY, (int) this.locZ, 0);

                    for (int i = 0; i < 1; ++i) {
                        PyromancyBlazeFireball entitysmallfireball = new PyromancyBlazeFireball(this.world, this, d0 + this.random.nextGaussian() * (double) f1, d1, d2 + this.random.nextGaussian() * (double) f1, null, skill);

                        entitysmallfireball.locY = this.locY + (double) (this.length / 2.0F) + 0.5D;
                        this.world.addEntity(entitysmallfireball);
                    }
                }
            }

            this.yaw = (float) (Math.atan2(d2, d0) * 180.0D / 3.1415927410125732D) - 90.0F;
            this.bn = true;
        }
        return;
    }

    public void setSpeed(float d) {
        this.bf = d;
    }

}
