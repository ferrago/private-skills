package net.kingdomsofarden.andrew2060.heroes.skills.pyromancy;

public class PathfinderGoalAttackTarget extends PathfinderGoalDeobfuscated {
    
    PyromancyBlaze blaze;
    
    public PathfinderGoalAttackTarget(PyromancyBlaze blaze) {
        this.blaze = blaze;
    }

    @Override
    public boolean canStart() {
        return true;
    }

    @Override
    public boolean canContinue() {
        return this.blaze.target != null;
    }

    @Override
    public void startGoal() {}

    @Override
    public void finishGoal() {
        if (this.blaze.target != null) {
            this.blaze.callAttackTick(this.blaze.target, this.blaze.target.e(this.blaze));
        }
    }


}
