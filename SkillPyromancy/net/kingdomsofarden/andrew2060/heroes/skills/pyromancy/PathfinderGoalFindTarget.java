package net.kingdomsofarden.andrew2060.heroes.skills.pyromancy;

public class PathfinderGoalFindTarget extends PathfinderGoalDeobfuscated {

    PyromancyBlaze blaze;
    private byte tickCounter;
    
    public PathfinderGoalFindTarget(PyromancyBlaze pyromancyBlaze) {
        this.blaze = pyromancyBlaze;
        this.tickCounter = -1;
    }

    @Override
    public boolean canStart() {
        this.tickCounter++;
        if (this.tickCounter > 100) { //prevent overflows
            this.tickCounter = 0;
        }
        return this.tickCounter % 7 == 0; //Only update targets once a second (pathfinders only get called 1/3 ticks, so 21 here) 
    }

    @Override
    public boolean canContinue() {
        return true;
    }

    @Override
    public void startGoal() {
        return;
    }

    @Override
    public void finishGoal() {
        this.blaze.target = this.blaze.findTarget();
    }


}
