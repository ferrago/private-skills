package net.kingdomsofarden.andrew2060.heroes.skills;

import java.lang.reflect.Field;
import java.text.DecimalFormat;

import net.kingdomsofarden.andrew2060.heroes.skills.SkillMageFire.MageFireDebuffEffect;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;

public class SkillCombust extends ActiveSkill {
    
    private Field expireTimeField;
    
    public SkillCombust(Heroes plugin) {
        super(plugin, "Combust");
        this.setDescription("Combusts all nearby entities, consuming and dealing $1 damage per MageFire stack. While in drake form, each stack consumed adds $2 seconds to the duration of drake form.");
        this.setArgumentRange(0, 0);
        this.setIdentifiers("skill combust");
        this.setUsage("/skill combust");
        try {
            expireTimeField = ExpirableEffect.class.getDeclaredField("expireTime");
            expireTimeField.setAccessible(true);
        } catch (NoSuchFieldException | SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public SkillResult use(Hero hero, String[] arg1) {
        double damagePerStack = SkillConfigManager.getUseSetting(hero, this, "damage-per-stack", 10, false);
        long durationPerStack = SkillConfigManager.getUseSetting(hero, this, "drake-duration-stack", 2000, false);

        int stacksConsumed = 0;
        for (Entity e : hero.getPlayer().getNearbyEntities(16, 16, 16)) {
            if (!(e instanceof LivingEntity)) {
                continue;
            }
            LivingEntity lE = (LivingEntity)e;
            CharacterTemplate cT = plugin.getCharacterManager().getCharacter(lE);
            MageFireDebuffEffect effect = (MageFireDebuffEffect) cT.getEffect("MageFireDebuff");
            if (effect == null) {
                continue;
            } else {
                if (Skill.damageCheck(hero.getPlayer(), lE)) {
                    int stacks = effect.getStacks();
                    this.addSpellTarget(lE, hero);
                    Skill.damageEntity(lE, hero.getPlayer(), stacks * damagePerStack, DamageCause.MAGIC, false);
                    cT.removeEffect(effect);
                    stacksConsumed += stacks;
                    cT.getEntity().setFireTicks(0);
                }
                continue;
            }
        }
        if (stacksConsumed > 0) {
            if (hero.hasEffect("Drake")) {   
                ExpirableEffect effect = (ExpirableEffect) hero.getEffect("Drake"); {
                    try {
                        long expireTime = expireTimeField.getLong(effect);
                        expireTime += durationPerStack * stacksConsumed;
                        expireTimeField.setLong(effect, expireTime);
                    } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return SkillResult.NORMAL;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("damage-per-stack", Integer.valueOf(10));
        node.set("drake-duration-stack", Long.valueOf(2000));
        return node;
    }
    
    @Override
    public String getDescription(Hero hero) {
        double damagePerStack = SkillConfigManager.getUseSetting(hero, this, "damage-per-stack", 10, false);
        double drakeDuration = (long) (SkillConfigManager.getUseSetting(hero, this, "drake-duration-stack", 2000, false) * 0.001);
        DecimalFormat dF = new DecimalFormat("##.##");
        return getDescription().replace("$1", dF.format(damagePerStack)).replace("$2", dF.format(drakeDuration));
    }

}
