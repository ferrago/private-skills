package net.kingdomsofarden.andrew2060.heroes.skills;

import java.text.DecimalFormat;
import net.kingdomsofarden.andrew2060.toolhandler.ToolHandlerPlugin;
import net.kingdomsofarden.andrew2060.toolhandler.potions.PotionEffectManager;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityCombustByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.potion.PotionEffectType;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import com.herocraftonline.heroes.characters.skill.SkillType;

public class SkillFireStorm extends ActiveSkill {
    
    public SkillFireStorm(Heroes plugin) {
        super(plugin, "FireStorm");
        setIdentifiers("skill firestorm");
        setUsage("/skill firestorm");
        setArgumentRange(0,0);
        setDescription("On use, user is rooted into place for $1 seconds and "
                + "unleashes a devastating magical fire storm in the surrounding area "
                + "that applies magefire stacks and deals $2 ($3 in drake form) per second");
        setTypes(SkillType.DAMAGING, SkillType.SILENCABLE, SkillType.HARMFUL, SkillType.FIRE);
    }

    @Override
    public SkillResult use(Hero hero, String[] arg1) {
        long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false) 
                + hero.getSkillLevel(this) * SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION_INCREASE, 500, false);
        hero.addEffect(new FireStormRootEffect(this, duration));
        broadcast(hero.getPlayer().getLocation(), getUseText(), hero.getPlayer().getName());
        return SkillResult.NORMAL;
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.USE_TEXT.node(), "§7[§2Skill§7] $1 has begun channeling a firestorm!");
        node.set(SkillSetting.DURATION.node(), Long.valueOf(5000));
        node.set(SkillSetting.DURATION_INCREASE.node(), Long.valueOf(100));
        node.set(SkillSetting.DAMAGE.node(), Double.valueOf(30));
        node.set(SkillSetting.DAMAGE_INCREASE.node(), Double.valueOf(0.25));
        node.set("damage-mult-drake", Double.valueOf(1.5));
        return node;
    }

    @Override
    public String getDescription(Hero hero) {
        double duration = (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false) 
                + hero.getSkillLevel(this) * SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION_INCREASE, 100, false)) * 0.001;
        DecimalFormat dF = new DecimalFormat("#0.##");
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 30, false) 
                + hero.getSkillLevel(this) * SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_INCREASE, 0.25, false);
        double drakeDamage = damage * SkillConfigManager.getUseSetting(hero, this, "damage-mult-drake", 1.5, false);
        return getDescription().replace("$1", dF.format(duration)).replace("$2", dF.format(damage)).replace("$3", dF.format(drakeDamage));
    }

    private class FireStormRootEffect extends PeriodicExpirableEffect {
        
        private int disp;
        
        public FireStormRootEffect(Skill skill, Long duration) {
            super(skill, "FirestormRoot", 1000, duration);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.BENEFICIAL);
            this.disp = 0;
        }
        
        @Override
        public void apply(CharacterTemplate cT) {
            LivingEntity lE = cT.getEntity();
            PotionEffectManager handler = ToolHandlerPlugin.instance.getPotionEffectHandler();
            handler.addPotionEffectStacking(PotionEffectType.SLOW.createEffect((int) (getDuration()*0.001*20), 128), lE, false);
            handler.addPotionEffectStacking(PotionEffectType.JUMP.createEffect((int) (getDuration()*0.001*20), -128), lE, false);
            super.apply(cT);
        }

        @Override
        public void tickHero(Hero h) {
            double damage = SkillConfigManager.getUseSetting(h, skill, SkillSetting.DAMAGE, 30, false) 
                    + h.getSkillLevel(skill) * SkillConfigManager.getUseSetting(h, skill, SkillSetting.DAMAGE_INCREASE, 0.25, false);
            if (h.hasEffect("Drake")) {
                damage *= SkillConfigManager.getUseSetting(h, skill, "damage-mult-drake", 1.5, false);
            }
            for (Entity e : h.getPlayer().getNearbyEntities(16, 5, 16)) {
                if (!(e instanceof LivingEntity)) {
                    continue;
                }
                if (!Skill.damageCheck(h.getPlayer(), (LivingEntity) e)) {
                    continue;
                }
                addSpellTarget(e,h);
                Skill.damageEntity((LivingEntity)e, h.getPlayer(), damage, DamageCause.MAGIC);
                EntityCombustByEntityEvent event = new EntityCombustByEntityEvent(h.getPlayer(), e, 40);
                plugin.getServer().getPluginManager().callEvent(event);
                if (!event.isCancelled()) {
                    e.setFireTicks(e.getFireTicks() > event.getDuration() * 20 ? e.getFireTicks() : event.getDuration() * 20);
                }
            }
            if (disp % 2 == 0) {
                Location loc = h.getPlayer().getLocation();
                loc.getWorld().spigot().playEffect(loc, Effect.MOBSPAWNER_FLAMES, 0, 0, 8f , 2.5f, 8f, 1.0f, 30000, 30);
            }
            disp++;
        }

        @Override
        public void tickMonster(Monster m) {
            return;
        }
        
        
    }
    
}
