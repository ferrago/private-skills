package net.kingdomsofarden.andrew2060.heroes.skills;

import java.text.DecimalFormat;

import net.kingdomsofarden.andrew2060.api.effects.ExclusiveEffectUtil;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.metadata.FixedMetadataValue;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;

public class SkillMarksman extends ActiveSkill implements Listener{

    public SkillMarksman(Heroes plugin) {
        super(plugin, "Marksman");
        setIdentifiers("skill marksman");
        setDescription("Stance: Arrows fired gain damage based on the range from which they are fired ($1% extra damage per block beyond $2 blocks). Arrows also fly 50% faster.");
        Bukkit.getServer().getPluginManager().registerEvents(this, this.plugin);
        plugin.getServer().getScheduler().runTask(plugin, new Runnable() {

            @Override
            public void run() {
                ExclusiveEffectUtil.registerExclusiveEffect("ArcherStance", "Marksman");
            }

        });
    }

    @Override
    public String getDescription(Hero h) {
        double multiplier = SkillConfigManager.getUseSetting(h, this, "damage-block", 0.05, false);
        int minrange = SkillConfigManager.getUseSetting(h, this, "block-threshold", 16, false);
        DecimalFormat dF = new DecimalFormat("##.##");
        return getDescription()
                .replace("$1", dF.format(multiplier * 100))
                .replace("$2", minrange + "");
    }

    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("damage-block", Double.valueOf(0.05));
        node.set("block-threshold", Integer.valueOf(16));
        return node;
    }

    @EventHandler(priority=EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntityShootBow(EntityShootBowEvent event) {
        CharacterTemplate cT = plugin.getCharacterManager().getCharacter(event.getEntity());
        if (cT.hasEffect("Marksman")) {
            Entity e = event.getProjectile();
            e.setVelocity(e.getVelocity().multiply(1.5));
            e.setMetadata("MarksmanSkill", new FixedMetadataValue(plugin, e.getLocation()));
        }
    }

    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled = true)
    public void onWeaponDamage(WeaponDamageEvent event) {
        if (!event.isProjectile()) {
            return;
        }
        Entity e = event.getAttackerEntity();
        if (!e.hasMetadata("MarksmanSkill")) {
            return;
        }
        CharacterTemplate h = event.getDamager();

        double dist = ((Location)e.getMetadata("MarksmanSkill").get(0).value()).distance(event.getEntity().getLocation());
        int minrange;
        double multiplier;
        if (h instanceof Hero) {
            minrange = SkillConfigManager.getUseSetting((Hero) h, this, "block-threshold", 16, false);
            multiplier = SkillConfigManager.getUseSetting((Hero) h, this, "damage-block", 0.05, false);
        } else {
            minrange = Integer.valueOf(SkillConfigManager.getRaw(this, "block-threshold", "16"));
            multiplier = Double.valueOf(SkillConfigManager.getRaw(this, "damage-block", "0.05"));
        }
        dist -= minrange;
        if (dist < 0) {
            dist = 0;
        }
        double addDamage = dist*multiplier;
        event.setDamage(event.getDamage()  * (1+addDamage));
    }

    @Override
    public SkillResult use(Hero hero, String[] arg1) {
        if (hero.hasEffect("Marksman")) {
            hero.getPlayer().sendMessage("Your active stance is already Marksman!");
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        hero.getPlayer().sendMessage("Stance changed to Marksman!");
        Effect e = new Effect(this, "Marksman");
        e.setPersistent(true);
        ExclusiveEffectUtil.addExclusiveEffect(hero, e, "ArcherStance");
        return SkillResult.NORMAL;
    }
}
