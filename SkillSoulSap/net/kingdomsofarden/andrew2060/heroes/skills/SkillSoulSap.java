package net.kingdomsofarden.andrew2060.heroes.skills;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.skill.PassiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;

public class SkillSoulSap extends PassiveSkill {

	public SkillSoulSap(Heroes plugin) {
		super(plugin, "SoulSap");
		setDescription("Passive: any player kills will automatically charge soul gems (emeralds) based on level if available in inventory");
		Bukkit.getServer().getPluginManager().registerEvents(new SoulSapListener(this), this.plugin);
	}

	@Override
	public String getDescription(Hero hero) {
		return getDescription();
	}
	public class SoulSapListener implements Listener {
		private Skill skill;

		public SoulSapListener(Skill skill) {
			this.skill = skill;
		}

		@EventHandler(priority = EventPriority.MONITOR)
		public void onPlayerDeath(PlayerDeathEvent event) {
			Player p = event.getEntity();
			Player k = p.getKiller();
			if (k==null) {
				return;
			}
			Hero h = skill.plugin.getCharacterManager().getHero(k);
			if (!h.hasEffect("SoulSap")) {
				return;
			}
			PlayerInventory pI = k.getInventory();
			if (!pI.contains(Material.EMERALD)) {
				return;
			}
			for (Map.Entry<Integer, ? extends ItemStack> entry : pI.all(Material.EMERALD).entrySet()) {
				int next = entry.getKey();
				ItemStack emerald = entry.getValue();
				if (emerald.getAmount() > 1) {
					if (!createSoulGem(pI,p,skill.plugin)) {
						k.sendMessage("Not enough space in your inventory for a new soul gem!");
						return;
					}
					emerald.setAmount(emerald.getAmount()-1);
					break;
				}
				if (!emerald.getItemMeta().hasDisplayName()) {
					if (!createSoulGem(pI,p,skill.plugin)) {
						k.sendMessage("Not enough space in your inventory for a new soul gem!");
						return;
					}
					break;
				}
				if (emerald.getItemMeta().getDisplayName().toUpperCase().contains("GEM")) {
					continue;
				}
				if (!createSoulGem(pI,p,skill.plugin)) {
					k.sendMessage("Not enough space in your inventory for a new soul gem!");
					return;
				}
                pI.clear(next);
				return;
			}
		}

		private boolean createSoulGem(PlayerInventory pI, Player deadPlayer, Heroes heroes) {
			int empty = pI.firstEmpty();
			if (empty == -1) {
				return false;
			}
			ItemStack emerald = new ItemStack(Material.EMERALD,1);
			Hero h = heroes.getCharacterManager().getHero(deadPlayer);
			int level = h.getLevel();
			String rank = "";
			if (level >= 0 && level <= 20) {
				rank = ChatColor.DARK_AQUA + "Petty";
			}
			if (level > 20 && level <= 30) {
				rank = ChatColor.AQUA + "Weak";
			}
			if (level > 30 && level <= 40) {
				rank = ChatColor.WHITE + "Common";
			}
			if (level > 40 && level <= 50) {
				rank = ChatColor.DARK_GREEN + "Strong";
			}
			if (level > 50 && level < 65) {
				rank = ChatColor.GOLD + "Major";
			}
			if (level >= 65 && level <75) {
				rank = ChatColor.RED + "Master";
			}
			if (level == 75) {
				rank = ChatColor.DARK_RED + "Legendary";
			}
			if (!h.getHeroClass().hasNoParents()) {
				rank = ChatColor.DARK_RED + "Legendary";
			}
			ItemMeta meta = emerald.getItemMeta();
			meta.setDisplayName(rank + " Soul Gem");
			List<String> lore = new ArrayList<String>();
			lore.add(0,ChatColor.GRAY + "A " + rank + "§7tier soul is trapped within this gem");
			meta.setLore(lore);
			emerald.setItemMeta(meta);
			pI.setItem(empty, emerald);
			((Player)((LivingEntity)pI.getHolder())).sendMessage(rank + " Soul Gem" + ChatColor.GRAY + " Successfully Created!");
			return true;
		}
	}

}
