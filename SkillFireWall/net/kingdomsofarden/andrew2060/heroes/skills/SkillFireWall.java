package net.kingdomsofarden.andrew2060.heroes.skills;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.Monster;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.PeriodicExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import net.kingdomsofarden.andrew2060.toolhandler.ToolHandlerPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import java.text.DecimalFormat;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class SkillFireWall extends ActiveSkill {
    
    private static double ninetyDegrees = Math.PI / 2;

    public SkillFireWall(Heroes plugin) {
        super(plugin, "FireWall");
        setIdentifiers("skill firewall", "fwall", "fireblast");
        setUsage("/skill firewall");
        setArgumentRange(0,0);
        setDescription("Normal: Summons a wall of fire that deals $1 damage and $2% 3 second slow to anyone passing " +
                "through it. Wall lasts for $3 seconds. Drake: knocks away everyone hostile in the vicinity, " +
                "dealing $4 damage, and sets them on fire");
    }

    @Override
    public SkillResult use(Hero hero, String[] args) {
        if (hero.hasEffect("Drake")) { //Drake morph is active, use alternate effects
            Player p = hero.getPlayer();
            double drakeDamage = SkillConfigManager.getUseSetting(hero, this, "damage-drake", 60, false);
            for (Entity e : p.getNearbyEntities(16, 5, 16)) {
                if (!(e instanceof LivingEntity)) {
                    continue;
                }
                if (damageCheck(p, (LivingEntity)e)) {
                    Vector v = e.getLocation().add(0, 2, 0).toVector().subtract(p.getLocation().toVector()).normalize();
                    v = v.multiply(4);
                    v = v.setY(0);
                    e.setVelocity(v);
                    p.getWorld().playEffect(p.getLocation(), Effect.ZOMBIE_CHEW_WOODEN_DOOR, 1);
                    this.addSpellTarget(e, hero);
                    this.damageEntity((LivingEntity)e, hero.getPlayer(), drakeDamage);
                    //TODO: Set on Fire
                }
            }
        } else {
            if (hero.hasEffect("FirewallPlacement")) {
                int width = SkillConfigManager.getUseSetting(hero, this, "width", 5, false);
                if (width % 2 == 1) { //Odd number of blocks, subtract one since we add one anyways
                    width -= 1;
                }
                width = (int) Math.floor(width / 2);
                Deque<Location> wallBase = getWallBase(hero.getPlayer(), width);
                Table<Integer,Integer,Set<Integer>> wallLocation = HashBasedTable.create(); 
                Location loc = wallBase.poll();
                int height = SkillConfigManager.getUseSetting(hero, this, "height", 3, false);
                UUID worldId = loc.getWorld().getUID();
                while (loc != null) {
                    for (int i = 1; i <= height; i++) {
                        Location add = loc.clone().add(0,i,0);
                        if (add.getBlock().getType().equals(Material.AIR)) {
                            int x = (int) Math.floor(add.getX());
                            int z = (int) Math.floor(add.getZ());
                            Set<Integer> y = wallLocation.get(x, z);
                            if (y != null) {
                                y.add((int) Math.floor(add.getY()));
                            } else {
                                wallLocation.put(x, z, new HashSet<Integer>());
                                wallLocation.get(x, z).add((int) Math.floor(add.getY()));
                            }
                        }
                    }                
                    loc = wallBase.poll();
                }
                final FireWallListener wallListener = new FireWallListener(this, worldId, wallLocation, hero);
                plugin.getServer().getPluginManager().registerEvents(wallListener, this.plugin);
                long duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false);
                final BukkitTask task = Bukkit.getScheduler().runTaskTimer(plugin, new Runnable() {

                    @Override
                    public void run() {
                    }
                    
                }, 0, 5);
                Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {

                    @Override
                    public void run() {
                        PlayerMoveEvent.getHandlerList().unregister(wallListener); 
                        task.cancel();
                    }
                    
                }, (long) (duration * 0.001 * 20));
            } else {
                hero.getPlayer().sendMessage(ChatColor.GRAY + "[" + ChatColor.GREEN + "Skill" + ChatColor.GRAY + "] FireWall Placement mode active for 20 seconds! Use again to place!");
                int width = SkillConfigManager.getUseSetting(hero, this, "width", 5, false);
                if (width % 2 == 1) { //Odd number of blocks, subtract one since we add one anyways
                    width -= 1;
                }
                width = (int) Math.floor(width / 2);
                hero.addEffect(new WallPlacementEffect(this, this.plugin, width));
            }
        }
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 30, false);
        int slow = SkillConfigManager.getUseSetting(hero, this, "slow-amount", 1, false);
        double duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 5000, false) * 0.001;
        double leviathanDamage = SkillConfigManager.getUseSetting(hero, this, "damage-drake", 60, false);
        DecimalFormat dF = new DecimalFormat("##.##");
        return getDescription().replace("$1", dF.format(damage)).replace("$2", new DecimalFormat("##").format(slow * 20)).replace("$3", dF.format(duration).replace("$4", dF.format(leviathanDamage)));
    }
    
    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), 30);
        node.set("slow-amount", 1);
        node.set(SkillSetting.DURATION.node(), 5000);
        node.set("damage-drake", 60);

        return node;
    }
    
    Deque<Location> getWallBase(Player player, int halfWidth) {
        @SuppressWarnings("deprecation")
        List<Block> los = player.getLineOfSight(new HashSet<Material>(), 32);
        if (los.isEmpty()) {
            return null;
        }
        Location target = los.get(0).getLocation();
        Deque<Location> wallBase = new LinkedList<Location>();
        Vector direction = target.subtract(player.getLocation()).toVector();
        double directionAngle = Math.atan(direction.getZ()/direction.getX());
        double directionLeftAngle = directionAngle + ninetyDegrees;
        for (int i = -halfWidth; i <= halfWidth; i++) {
            double x = Math.cos(directionLeftAngle) * i;
            double z = Math.cos(directionLeftAngle) * i;
            Location add = target.clone().add(x,0,z);
            add.setX(Math.floor(add.getX()));
            add.setZ(Math.floor(add.getY()));
            Location previous = wallBase.peek();
            if (previous != null) {
                if (previous.getX() == add.getX() && previous.getZ() == add.getZ()) {
                    continue;
                }
            } else {
                previous = add;
            }
            Block check = add.getBlock();
            if (check.getType().equals(Material.AIR)) { //Empty block
                if (!check.getRelative(BlockFace.UP).getType().equals(Material.AIR)) { //Not empty above, 1 block raise?
                    if (check.getRelative(BlockFace.UP,2).getType().equals(Material.AIR)) { //1 block raise, follow
                        if (check.getRelative(BlockFace.DOWN).getType().equals(Material.AIR)) { //Empty block below
                            add.setY(Math.floor(previous.getY() + 1));
                        } else {
                            add.setY(Math.floor(previous.getY() - 1));
                        }
                    } else { //Greater than 1 block raise, check down
                        if (check.getRelative(BlockFace.DOWN).getType().equals(Material.AIR)) { //Empty block below
                            continue; //skip
                        } else {
                            add.setY(Math.floor(previous.getY() - 1));
                        }
                    }
                } else { //Empty above, check below
                    if (!check.getRelative(BlockFace.DOWN).getType().equals(Material.AIR)) { //1 block down, follow
                        add.setY(Math.floor(previous.getY() - 1));
                    } else {
                        continue; //skip
                    }
                }
            } else { //Not empty -> check upwards only
                if (!check.getRelative(BlockFace.UP).getType().equals(Material.AIR)) {
                    if (check.getRelative(BlockFace.UP,2).getType().equals(Material.AIR)) { //1 block raise, follow
                        add.setY(check.getRelative(BlockFace.UP).getY());
                    } else { //Greater than a 1 block raise, do not follow
                        continue; //skip
                    }
                }
            }
            wallBase.push(add);
        }
        return wallBase;
    }
    
    public class WallPlacementEffect extends PeriodicExpirableEffect {
        
        private int width;

        public WallPlacementEffect(Skill skill, Heroes plugin, int width) {
            super(skill, plugin, "FirewallPlacement", 500, 20000);
            this.width = width;
        }

        @SuppressWarnings("deprecation")
        @Override
        public void tickHero(final Hero hero) {
            final Deque<Location> wallBase = getWallBase(hero.getPlayer(),width);
            for (Location loc : wallBase) {
                hero.getPlayer().sendBlockChange(loc, Material.GLOWSTONE, (byte) 0);
            }
            plugin.getServer().getScheduler().runTaskLater(plugin, new Runnable() {

                @Override
                public void run() {
                    for (Location loc : wallBase) {
                        Block b = loc.getBlock();
                        hero.getPlayer().sendBlockChange(loc, b.getType(), b.getData());
                    } 
                }
                
            }, 9);
        }

        @Override
        public void tickMonster(Monster arg0) {
            return;
        }
        
        
    }
    
    public class FireWallListener implements Listener {
        
        private UUID worldId;
        private Table<Integer,Integer,Set<Integer>> wallLocation;
        private Hero hero;
        private Skill skill;
        private String effectId;
        
        public FireWallListener(Skill skill, UUID world, Table<Integer,Integer,Set<Integer>> wall, Hero applier) {
            this.skill = skill;
            this.worldId = world;
            this.wallLocation = wall;
            this.hero = applier;
            this.effectId = UUID.randomUUID().toString();
        }
        
        @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
        public void onPlayerMove(PlayerMoveEvent event) {
            Location loc = event.getPlayer().getLocation();
            if (!loc.getWorld().getUID().equals(worldId)) {
                return;
            }
            if (!damageCheck(hero.getPlayer(), event.getPlayer())) {
                return;
            }
            Hero h = plugin.getCharacterManager().getHero(event.getPlayer()); 
            if (h.hasEffect(effectId)) {
                int x = (int) Math.floor(loc.getX());
                int z = (int) Math.floor(loc.getZ());
                Set<Integer> y = wallLocation.get(x, z);
                if (y == null) {
                    h.removeEffect(h.getEffect(effectId));
                    return;
                } else if (y.contains(Math.floor(loc.getY()))) {
                    return;
                } else {
                    h.removeEffect(h.getEffect(effectId));
                    return;
                }
            } else {
                int x = (int) Math.floor(loc.getX());
                int z = (int) Math.floor(loc.getZ());
                Set<Integer> y = wallLocation.get(x, z);
                if (y == null) {
                    return;
                } else if (y.contains(Math.floor(loc.getY()))) {
                    double damage = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE, 30, false);
                    int slow = SkillConfigManager.getUseSetting(hero, skill, "slow-amount", 1, false);
                    long duration = SkillConfigManager.getUseSetting(hero, skill, "duration", 5000, false);
                    Player p = event.getPlayer();
                    skill.addSpellTarget(event.getPlayer(), hero);
                    Skill.damageEntity(event.getPlayer(), hero.getEntity(), damage, DamageCause.MAGIC);
                    ToolHandlerPlugin.instance.getPotionEffectHandler()
                            .addPotionEffectStacking(PotionEffectType.SLOW.createEffect(60, slow), p, false);
                    h.addEffect(new ExpirableEffect(skill,effectId,duration));
                } else {
                    return;
                }
            }
            
        }
    }
    
    public class FireWallDisplayTask extends BukkitRunnable {
        private Table<Integer,Integer,Set<Integer>> wallLocation;
        private Location baseLoc;
        
        public FireWallDisplayTask(Table<Integer,Integer,Set<Integer>> wallLocation, Location base) {
            this.wallLocation = wallLocation;
            this.baseLoc = base;
        }
        
        @SuppressWarnings("deprecation")
        public void run() {
            List<Location> display = new LinkedList<Location>();
            World w = baseLoc.getWorld();
            Map<Integer, Map<Integer,Set<Integer>>> xzy = wallLocation.rowMap();
            for (Integer x : xzy.keySet()) {
                Map<Integer,Set<Integer>> zy = xzy.get(x);
                for (Integer z : zy.keySet()) {
                    for (Integer y : zy.get(z)) {
                        Location loc = new Location(w, x, y, z);
                        display.add(loc);
                    }
                }
            }
            for (Entity e : this.baseLoc.getChunk().getEntities()) {
                if (e instanceof Player) {
                    Player p = (Player)e;
                    for (Location loc : display) {
                        p.sendBlockChange(loc, Material.FIRE, (byte)0);
                    }
                }
            }
        }
        
        @Override
        @SuppressWarnings("deprecation")
        public void cancel() {
            List<Location> display = new LinkedList<Location>();
            World w = baseLoc.getWorld();
            Map<Integer, Map<Integer,Set<Integer>>> xzy = wallLocation.rowMap();
            for (Integer x : xzy.keySet()) {
                Map<Integer,Set<Integer>> zy = xzy.get(x);
                for (Integer z : zy.keySet()) {
                    for (Integer y : zy.get(z)) {
                        Location loc = new Location(w, x, y, z);
                        display.add(loc);
                    }
                }
            }
            for (Entity e : this.baseLoc.getChunk().getEntities()) {
                if (e instanceof Player) {
                    Player p = (Player)e;
                    for (Location loc : display) {
                        p.sendBlockChange(loc, w.getBlockAt(loc).getType(), (byte)0);
                    }
                }
            }
            super.cancel();
        }
    }
    
    
    
}
