package net.kingdomsofarden.andrew2060.heroes.skills;

import java.util.Map;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.EffectType;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillType;
import com.herocraftonline.heroes.characters.skill.SkillSetting;

import net.kingdomsofarden.andrew2060.toolhandler.ToolHandlerPlugin;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public class SkillCompress extends ActiveSkill implements Listener {

    public SkillCompress(Heroes plugin) {
        super(plugin, "Compress");
        setDescription("Passive: Attacks will root a target every 5 seconds for 1 second and deal extra damage. " +
                "Active: Your tools exert extra pressure as you mine for $1 seconds, compressing materials");
        setUsage("/skill compress");
        setArgumentRange(0, 0);
        setIdentifiers("skill compress");
        setTypes(SkillType.EARTH, SkillType.BUFF, SkillType.SILENCABLE);
        Bukkit.getServer().getPluginManager().registerEvents(this, plugin);
    }

    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection section = super.getDefaultConfig();
        section.set(SkillSetting.DURATION.node(), Integer.valueOf(10000));
        section.set(SkillSetting.APPLY_TEXT.node(), "%hero%'s tools now exert extreme pressure " +
                "on anything they contact!");
        section.set(SkillSetting.EXPIRE_TEXT.node(), "%hero%'s tools are no longer exerting pressure!");
        return section;
    }

    public void init() {
        super.init();
    }

    public SkillResult use(Hero hero, String[] args) {
        broadcastExecuteText(hero);

        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        hero.addEffect(new CompressEffect(this, duration));

        return SkillResult.NORMAL;
    }

    public String getDescription(Hero hero) {
        int duration = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DURATION, 10000, false);
        return getDescription().replace("$1", duration / 1000 +"");
    }

    public class CompressEffect extends ExpirableEffect {
        public CompressEffect(Skill skill, long duration) {
            super(skill, "Compress", duration);
            this.types.add(EffectType.DISPELLABLE);
            this.types.add(EffectType.BENEFICIAL);
            this.types.add(EffectType.FIRE);
        }

        public void applyToHero(Hero hero) {
            super.applyToHero(hero);
            broadcast(hero.getPlayer().getLocation(), "$1's tools now exert extreme pressure on anything they contact!", new Object[] {hero.getName()});
        }

        public void removeFromHero(Hero hero) {
            super.removeFromHero(hero);
            broadcast(hero.getPlayer().getLocation(), "$1's tools are no longer exerting pressure!", new Object[] {hero.getName()});

        }
    }

    @EventHandler(priority=EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent event) {
        if (event.isCancelled()) {
            return;
        }

        Hero hero = SkillCompress.this.plugin.getCharacterManager().getHero(event.getPlayer());
        if (hero.hasEffect("Compress")) {
            Block block = event.getBlock();
            Material tool = event.getPlayer().getItemInHand().getType();
            Material type = block.getType();
            switch (tool) {
            case DIAMOND_SPADE:
            case IRON_SPADE:
            case GOLD_SPADE:
            case STONE_SPADE:
            case WOOD_SPADE:			    
                switch (type) {
                case SAND:
                    event.setCancelled(true);
                    block.setType(Material.AIR);
                    block.getWorld().dropItem(block.getLocation(), new ItemStack(Material.SANDSTONE, 1));
                    break;
                case CLAY:
                    event.setCancelled(true);
                    block.setType(Material.AIR);
                    block.getWorld().dropItem(block.getLocation(), new ItemStack(Material.CLAY_BRICK, 4));
                    break;
                default:
                    break;
                }

            case DIAMOND_PICKAXE:
            case IRON_PICKAXE:
            case GOLD_PICKAXE:
            case STONE_PICKAXE:
            case WOOD_PICKAXE:
                switch (type) {
                
                case STONE:
                    event.setCancelled(true);
                    block.setType(Material.AIR);
                    block.getWorld().dropItem(block.getLocation(), new ItemStack(Material.SMOOTH_BRICK, 1));
                    break;
                case NETHERRACK:
                    event.setCancelled(true);
                    block.setType(Material.AIR);
                    block.getWorld().dropItem(block.getLocation(), new ItemStack(Material.NETHER_BRICK, 1));
                    break;
                case GLOWSTONE:
                    event.setCancelled(true);
                    block.setType(Material.AIR);
                    block.getWorld().dropItem(block.getLocation(), new ItemStack(Material.GLOWSTONE_DUST, 8));
                    break;
                default:
                    break;
                    
                }
            case DIAMOND_AXE:
            case IRON_AXE:
            case GOLD_AXE:
            case STONE_AXE:
            case WOOD_AXE:
                switch (type) {
                case LOG:
                    event.setCancelled(true);
                    block.setType(Material.AIR);
                    block.getWorld().dropItem(block.getLocation(), new ItemStack(Material.COAL, 1));
                    break;
                default:
                    break;
                }
                break;
            default:
                break;
            }
            Map<Material, Double> expValues = Heroes.properties.miningExp;
            Double amount = expValues.get(type);
            if (amount == null) {
                amount = 0.00;
            }
            hero.addExp(amount , hero.getHeroClass(), hero.getPlayer().getLocation());
        }

    }


    @EventHandler(priority=EventPriority.HIGHEST)
    public void onWeaponDamage(WeaponDamageEvent event) {
        if (event.isCancelled()) {
            return;
        }
        if (event.getCause() != DamageCause.ENTITY_ATTACK) {
            return;
        }
        if ((event.getDamager() instanceof Hero) && (event.getEntity() instanceof LivingEntity)) {
            Hero hero = (Hero) event.getDamager();
            if (hero.hasAccessToSkill("Compress")) {
                CharacterTemplate cT = plugin.getCharacterManager().getCharacter((LivingEntity) event.getEntity());
                if (!cT.hasEffect("CompressCooldown")) {
                    cT.addEffect(new CompressCooldownEffect(this, this.plugin));
                    event.setCancelled(true);
                    if (Skill.damageCheck(hero.getPlayer(), cT.getEntity())) {
                        addSpellTarget(cT.getEntity(), hero);
                        Skill.damageEntity(cT.getEntity(), hero.getEntity(),
                                event.getDamage(), DamageCause.ENTITY_ATTACK, false);
                        ToolHandlerPlugin.instance
                                .getPotionEffectHandler().addPotionEffectStacking(
                                PotionEffectType.SLOW.createEffect(50, 10), cT.getEntity(), false);
                        ToolHandlerPlugin.instance
                                .getPotionEffectHandler().addPotionEffectStacking(
                                PotionEffectType.JUMP.createEffect(50, -3), cT.getEntity(), false);
                        Location particleLoc = cT.getEntity().getLocation();
                        particleLoc.getWorld().spigot()
                                .playEffect(particleLoc, Effect.MAGIC_CRIT, 0, 0, 2, 1, 2, 1, 5, 16);
                        particleLoc.getWorld().playEffect(particleLoc, Effect.ZOMBIE_CHEW_IRON_DOOR, null);
                        addSpellTarget(cT.getEntity(), hero);
                        Skill.damageEntity(cT.getEntity(), hero.getEntity(), 10.0, DamageCause.MAGIC, false);
                    }
                }
            }
        }
    }

    private class CompressCooldownEffect extends ExpirableEffect {
        public CompressCooldownEffect(Skill skill, Heroes plugin) {
            super(skill, plugin, "CompressCooldown", 6000);
        }

    }
}