package net.kingdomsofarden.andrew2060.heroes.skills;

import java.text.DecimalFormat;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import net.kingdomsofarden.andrew2060.api.effects.ExclusiveEffectUtil;
import net.kingdomsofarden.andrew2060.toolhandler.ToolHandlerPlugin;
import net.kingdomsofarden.andrew2060.toolhandler.potions.PotionEffectManager;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.PeriodicEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;

public class SkillRedoubt extends ActiveSkill implements Listener {
    
    private static double fifteenDegrees_Radians;
    
    static {
        fifteenDegrees_Radians = Math.PI/12;
    }

    public SkillRedoubt(Heroes plugin) {
        super(plugin, "Redoubt");
        setIdentifiers("skill redoubt");
        setUsage("/skill redoubt");
        setArgumentRange(0,0);
        setDescription("Stance: Arrows travel $1% slower but deal $2% damage, losing $3% damage per block traveled past $4 blocks (min 10% damage). "
                + "While in this stance, shots will automatically volley, but the player is rendered immobile.");
        plugin.getServer().getScheduler().runTask(plugin, new Runnable() {

            @Override
            public void run() {
                ExclusiveEffectUtil.registerExclusiveEffect("ArcherStance", "Redoubt");
            }
            
        });
    }

    @Override
    public SkillResult use(Hero hero, String[] arg1) {
        if (hero.hasEffect("Redoubt")) {
            hero.getPlayer().sendMessage("Your active stance is already Redoubt!");
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        hero.getPlayer().sendMessage(ChatColor.GRAY + "Redoubt stance is now active!");
        ExclusiveEffectUtil.addExclusiveEffect(hero, new RedoubtStanceEffect(this), "ArcherStance");
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        double speedReduc = SkillConfigManager.getUseSetting(hero, this, "speed-reduc", 0.25, false) * 100;
        double damageBoost = SkillConfigManager.getUseSetting(hero, this, "damage-boost", 2, false) * 100;
        double damageLoss = SkillConfigManager.getUseSetting(hero, this, "damage-loss", 0.1, false) * 100;
        int blockThreshold = SkillConfigManager.getUseSetting(hero, this, "block-threshold", 5, false);
        DecimalFormat dF = new DecimalFormat("##.##");
        return getDescription().replace("$1", dF.format(speedReduc))
                .replace("$2", dF.format(damageBoost))
                .replace("$3", dF.format(damageLoss))
                .replace("$4", blockThreshold + "");
    }
    
    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("speed-reduc", Double.valueOf(0.25));
        node.set("damage-boost", Double.valueOf(2));
        node.set("damage-loss", Double.valueOf(0.1));
        node.set("block-threshold", Integer.valueOf(5));
        return node;
    }
    
    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled = true)
    public void onWeaponDamage(WeaponDamageEvent event) {
        if (!event.isProjectile()) {
            return;
        }
        Entity e = event.getAttackerEntity();
        if (!e.hasMetadata("SkillRedoubt")) {
            return;
        }
        if (!(event.getDamager() instanceof LivingEntity)) {
            return;
        }
        CharacterTemplate cT = (CharacterTemplate)event.getDamager();
        int threshold;
        double multiplier;
        double damageBoost;
        if (cT instanceof Hero) {
            threshold = SkillConfigManager.getUseSetting((Hero) cT, this, "block-threshold", 5, false);
            damageBoost = SkillConfigManager.getUseSetting((Hero) cT, this, "damage-boost", 2, false);
            multiplier =  SkillConfigManager.getUseSetting((Hero) cT, this, "damage-loss", 0.1, false);
        } else {
            threshold = Integer.valueOf(SkillConfigManager.getRaw(this, "block-threshold", "5"));
            damageBoost = Double.valueOf(SkillConfigManager.getRaw(this, "damage-boost", "2"));
            multiplier = Double.valueOf(SkillConfigManager.getRaw(this, "damage-loss", "0.1"));
        }
        Location fromLoc = (Location) e.getMetadata("SkillRedoubt").get(0).value();
        double dist = fromLoc.distance(e.getLocation());
        dist -= threshold;
        if (dist < 0) {
            dist = 0;
        }
        double subDamage = dist*multiplier;
        if (subDamage > 0.9) {
            subDamage = 0.9;
        }
        
        event.setDamage(event.getDamage() * damageBoost  * (1 - subDamage));
    }
    
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntityShootBow(EntityShootBowEvent event) {
        if (!(event.getEntity() instanceof LivingEntity)) {
            return;
        }
        CharacterTemplate cT = plugin.getCharacterManager().getCharacter(event.getEntity());
        if (cT.hasEffect("Redoubt")) {
            double reduc = 1;
            if (cT instanceof Hero) {
                reduc -= SkillConfigManager.getUseSetting((Hero)cT, this, "speed-reduc", 0.25, false);
            } else {
                reduc -= Double.valueOf(SkillConfigManager.getRaw(this, "speed-reduc", "0.25"));
            }
            if (reduc < 0.1) {
                reduc = 0.1;
            }
            Entity e = event.getProjectile();
            Vector v = e.getVelocity().multiply(reduc);
            e.setVelocity(v);
            Location loc = e.getLocation();
            e.setMetadata("SkillRedoubt", new FixedMetadataValue(plugin, loc));
            double dX = v.getX();
            double dZ = v.getZ();
            double hypotenuse = Math.sqrt(Math.pow(dX,2) + Math.pow(dZ,2));
            double angle = Math.atan(dZ/dX);
            for (int i = 1; i <= 2; i++) {
                
                Vector vPositiveAngle = v.clone();
                Vector vNegativeAngle = v.clone();

                double adjustedAnglePositive = angle + fifteenDegrees_Radians*i;
                double adjustedAngleNegative = angle - fifteenDegrees_Radians*i;
                
                double zPositiveMultiplier = Math.sin(adjustedAnglePositive);
                double zNegativeMultiplier = Math.sin(adjustedAngleNegative);
                double xPositiveMultiplier = Math.cos(adjustedAnglePositive);
                double xNegativeMultiplier = Math.cos(adjustedAngleNegative);
                
                vPositiveAngle.setX(hypotenuse * xPositiveMultiplier);
                vPositiveAngle.setZ(hypotenuse * zPositiveMultiplier);
                
                vNegativeAngle.setX(hypotenuse * xNegativeMultiplier);
                vNegativeAngle.setZ(hypotenuse * zNegativeMultiplier);  
                
                Arrow positiveAngle = cT.getEntity().launchProjectile(Arrow.class);
                Location teleport = loc.clone().setDirection(vPositiveAngle);
                positiveAngle.teleport(teleport);
                positiveAngle.setVelocity(vPositiveAngle);
                positiveAngle.setMetadata("SkillRedoubt", new FixedMetadataValue(plugin, teleport));
                
                Arrow negativeAngle = cT.getEntity().launchProjectile(Arrow.class);
                teleport = loc.clone().setDirection(vNegativeAngle);
                negativeAngle.teleport(teleport);
                negativeAngle.setVelocity(vNegativeAngle);
                negativeAngle.setMetadata("SkillRedoubt", new FixedMetadataValue(plugin, teleport));
            }
        }
    }
    
    public class RedoubtStanceEffect extends PeriodicEffect {

        public RedoubtStanceEffect(Skill skill) {
            super(skill, "Redoubt", 500);
            this.setPersistent(true);
        }
        
        @Override
        public void tick(CharacterTemplate cT) {
            PotionEffectManager pMan = ToolHandlerPlugin.instance.getPotionEffectHandler();
            pMan.addPotionEffectStacking(PotionEffectType.JUMP.createEffect(10, -128), cT.getEntity(), false);
            super.tick(cT);
        }

        @Override
        public void tickHero(Hero h) {
            h.getPlayer().setWalkSpeed(0f);
        }

        @Override
        public void removeFromHero(Hero h) {
            h.getPlayer().setWalkSpeed(1f);
        }
        
    }

}
