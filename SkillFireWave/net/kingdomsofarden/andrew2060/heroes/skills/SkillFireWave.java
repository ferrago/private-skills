package net.kingdomsofarden.andrew2060.heroes.skills;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.common.CombustEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import net.kingdomsofarden.andrew2060.api.entity.EntityUtil;
import net.minecraft.server.v1_8_R1.DamageSource;
import net.minecraft.server.v1_8_R1.EntityLargeFireball;
import net.minecraft.server.v1_8_R1.EntityPlayer;
import net.minecraft.server.v1_8_R1.MovingObjectPosition;
import net.minecraft.server.v1_8_R1.World;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.craftbukkit.v1_8_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LargeFireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityCombustByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

import java.text.DecimalFormat;

public class SkillFireWave extends ActiveSkill {
    
    public SkillFireWave(Heroes plugin) {
        super(plugin, "Firewave");
        setIdentifiers("skill firewave", "skill fwave");
        setUsage("/skill firewave");
        setArgumentRange(0,0);
        setDescription("Triggers a wave of fire, sending fireballs flying in all directions dealing $1 damage. Drake: the wave produces an additional knockback effect.");
        plugin.getServer().getScheduler().runTask(plugin, new Runnable() {    
            @Override
            public void run() {
                EntityUtil.registerCustomEntity(SkillLargeFireball.class, "SkillFirewaveLarge", 12, false);
            }
        });
    }
    
    
    
    public static class SkillLargeFireball extends EntityLargeFireball {

        private Hero hero;
        private Skill skill;

        public SkillLargeFireball(World world) {
            super(world);
            this.die();
        }
        
        public SkillLargeFireball(World world, EntityPlayer player, double x, double y, double z, Hero hero, Skill skill) {
            super(world,player,x,y,z);
            this.hero = hero;
            this.skill = skill;
        }

        public boolean damageEntity(DamageSource damagesource, float f) {
            return false; //Invulnerable, prevent reflection
        }
        
        public void a(MovingObjectPosition movingobjectposition) {
            try {
                if (!this.world.isStatic) {
                    if (movingobjectposition.entity != null) {
                        Entity target = movingobjectposition.entity.getBukkitEntity();
                        if (!(target instanceof LivingEntity)) {
                            return;
                        }
                        if (Skill.damageCheck(hero.getPlayer(), (LivingEntity) target)) {
                            int fireLength = (int) Math.ceil(SkillConfigManager.getUseSetting(hero, skill, "fire-ticks", 100, false)/20);
                            EntityCombustByEntityEvent event = new EntityCombustByEntityEvent(hero.getPlayer(), target, fireLength);
                            Heroes.getInstance().getServer().getPluginManager().callEvent(event);
                            if (!event.isCancelled()) {
                                target.setFireTicks(event.getDuration() * 20);
                                Heroes.getInstance().getCharacterManager().getCharacter((LivingEntity) target).addEffect(new CombustEffect(skill, (Player) hero.getPlayer()));
                                skill.addSpellTarget(target, hero);
                                double damage = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE, 4, false);
                                damage += (SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE_INCREASE, 0.0, false) * hero.getSkillLevel(skill));
                                Skill.damageEntity((LivingEntity) target, hero.getPlayer(), damage, DamageCause.MAGIC, false);
                                if (hero.hasEffect("Drake")) {
                                    target.setVelocity(new Vector(this.motX, this.motY+0.5, this.motZ));
                                }
                            }
                        }
                    }

                    this.die();
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
                this.die();
            }
        }
    }

    @Override
    public SkillResult use(Hero hero, String[] arg1) {
        int numFireballs = SkillConfigManager.getUseSetting(hero, this, "fireballs", 6, false);
        numFireballs += (SkillConfigManager.getUseSetting(hero, this, "fireballs-per-level", .2, false) * hero.getSkillLevel(this));

        double diff = 2 * Math.PI / numFireballs;
        for (double a = 0; a < 2 * Math.PI; a += diff) {
            Vector vel = new Vector(Math.cos(a), 0, Math.sin(a));
            LargeFireball ball = shootFireball(hero);
            ball.setVelocity(vel);
            ball.setDirection(vel);
        }
        broadcastExecuteText(hero);
        return SkillResult.NORMAL;
    }
    
    public LargeFireball shootFireball(Hero hero) {
        Player p = hero.getPlayer();
        Location spawnLoc = p.getEyeLocation();
        CraftWorld cWorld = (CraftWorld) spawnLoc.getWorld();
        Vector direction = spawnLoc.getDirection().multiply(10);
        SkillLargeFireball fireball = new SkillLargeFireball(cWorld.getHandle(), ((CraftPlayer)p).getHandle(), direction.getX(), direction.getY(), direction.getZ(), hero, this);
        fireball.projectileSource = p;
        fireball.setPositionRotation(spawnLoc.getX(), spawnLoc.getY(), spawnLoc.getZ(), spawnLoc.getYaw(), spawnLoc.getPitch());
        cWorld.getHandle().addEntity(fireball);
        return (LargeFireball) fireball.getBukkitEntity();
    }

    @Override
    public String getDescription(Hero hero) {
        double damage = SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE, 4, false);
        damage += (SkillConfigManager.getUseSetting(hero, this, SkillSetting.DAMAGE_INCREASE, 0.0, false) * hero.getSkillLevel(this));
        return getDescription().replace("$1",(new DecimalFormat("#0.##")).format(damage));
    }
    
    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set(SkillSetting.DAMAGE.node(), Double.valueOf(4));
        node.set(SkillSetting.DAMAGE_INCREASE.node(), Double.valueOf(0.00));
        node.set("fireballs", Integer.valueOf(10));
        node.set("fireballs-per-level", Double.valueOf(0.2));
        return node;
    }
}
