package net.kingdomsofarden.andrew2060.heroes.skills;

import java.text.DecimalFormat;

import net.kingdomsofarden.andrew2060.api.effects.ExclusiveEffectUtil;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.metadata.FixedMetadataValue;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.Effect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;

public class SkillGuardian extends ActiveSkill implements Listener {

    public SkillGuardian(Heroes plugin) {
        super(plugin, "Guardian");
        setIdentifiers("skill guardian");
        setUsage("/skill guardian");
        setArgumentRange(0,0);
        setDescription("Stance: All arrow damage increased by $1%");
        
        plugin.getServer().getScheduler().runTask(plugin, new Runnable() {

            @Override
            public void run() {
                ExclusiveEffectUtil.registerExclusiveEffect("ArcherStance", "Guardian");
            }
            
        });
    }

    @Override
    public SkillResult use(Hero hero, String[] arg1) {
        if (hero.hasEffect("Guardian")) {
            hero.getPlayer().sendMessage("You are already in the Guardian stance!");
            return SkillResult.INVALID_TARGET_NO_MSG;
        }
        hero.getPlayer().sendMessage(ChatColor.GRAY + "Guardian stance is now active!");
        Effect e = new Effect(this, "Guardian");
        e.setPersistent(true);
        ExclusiveEffectUtil.addExclusiveEffect(hero, e, "ArcherStance");
        return SkillResult.NORMAL;
    }

    @Override
    public String getDescription(Hero hero) {
        double prot = SkillConfigManager.getUseSetting(hero, this, "dmg-bonus", 0.2, false);
        return getDescription().replace("$1", (new DecimalFormat("##.##")).format(prot * 100));
    }
    
    @Override
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("dmg-bonus", Double.valueOf(0.2));
        return node;
    }

    @EventHandler(priority=EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntityShootBow(EntityShootBowEvent event) {
        CharacterTemplate cT = plugin.getCharacterManager().getCharacter(event.getEntity());
        if (cT.hasEffect("Guardian")) {
            Entity e = event.getProjectile();
            e.setMetadata("GuardianSkill", new FixedMetadataValue(plugin, true));
        }
    }
    
    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled = true)
    public void onWeaponDamage(WeaponDamageEvent event) {
        if (!event.isProjectile()) {
            return;
        }
        Entity e = event.getAttackerEntity();
        if (!e.hasMetadata("GuardianSkill")) {
            return;
        }
        CharacterTemplate cT = event.getDamager();

        double multiplier;
        if (cT instanceof Hero) {
            multiplier = SkillConfigManager.getUseSetting((Hero) cT, this, "dmg-bonus", 0.2, false);
        } else {
            multiplier = Double.valueOf(SkillConfigManager.getRaw(this, "dmg-bonus", "0.2"));
        }
        
        event.setDamage(event.getDamage()  * (1+multiplier));
    }
}
