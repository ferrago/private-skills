package net.kingdomsofarden.andrew2060.heroes.skills;

import java.text.DecimalFormat;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustByEntityEvent;
import org.bukkit.projectiles.ProjectileSource;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.events.HeroRegainHealthEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.skill.PassiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;

public class SkillMageFire extends PassiveSkill implements Listener {

    public SkillMageFire(Heroes plugin) {
        super(plugin, "MageFire");
        this.setDescription("Fire damage done by this player applies 1 stack of mage fire (up to $1 stacks) and refreshes it for $2 seconds ($3 if in drake form). Each stack reduces healing by $4%");
        this.plugin.getServer().getPluginManager().registerEvents(this, this.plugin);
    }

    @Override
    public String getDescription(Hero hero) {
        double duration = Long.valueOf(SkillConfigManager.getRaw(this, "duration", "15000").replaceAll("[^0-9]", "")) * 0.001;
        double drakeDuration = Long.valueOf(SkillConfigManager.getRaw(this, "drake-duration", "30000").replaceAll("[^0-9]", "")) * 0.001;
        double healReduc = Double.valueOf(SkillConfigManager.getRaw(this, "heal-reduc-per-stack", "0.02")) * 100;
        int maxStacks = Integer.valueOf(SkillConfigManager.getRaw(this, "max-stacks", "10").replaceAll("[^0-9]", ""));
        DecimalFormat dF = new DecimalFormat("##.##");
        return getDescription().replace("$1", maxStacks + "")
                .replace("$2", dF.format(duration))
                .replace("$3", dF.format(drakeDuration))
                .replace("$4", dF.format(healReduc));
    }
    
    public ConfigurationSection getDefaultConfig() {
        ConfigurationSection node = super.getDefaultConfig();
        node.set("duration", Long.valueOf(15000));
        node.set("drake-duration", Long.valueOf(30000));
        node.set("heal-reduc-per-stack", Double.valueOf(0.02));
        node.set("max-stacks", Integer.valueOf(10));
        return node;
    }
    
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onHeroRegainHealth(HeroRegainHealthEvent event) {
        if (event.getHero().hasEffect("MageFireDebuff")) {
            MageFireDebuffEffect effect = (MageFireDebuffEffect) event.getHero().getEffect("MageFireDebuff");
            int stacks = effect.getStacks();
            double healReduc = Double.valueOf(SkillConfigManager.getRaw(this, "heal-reduc-per-stack", "0.02").replaceAll("[^0-9.]", "")) * stacks;
            if (healReduc > 1) {
                healReduc = 1;
            }
            event.setAmount(event.getAmount() * (1 - healReduc));
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntityCombust(EntityCombustByEntityEvent event) {
        if (!(event.getEntity() instanceof LivingEntity)) {
            return;
        }
        Entity e = event.getCombuster();
        if (e instanceof Projectile) {
            ProjectileSource shooter = ((Projectile) e).getShooter();
            if (shooter instanceof LivingEntity) {
                CharacterTemplate cT = plugin.getCharacterManager().getCharacter((LivingEntity) shooter);
                if (cT.hasEffect("MageFire")) {
                    applyMageFireStack((LivingEntity) event.getEntity(), cT);
                    return;
                }
            }
        } else if (e instanceof LivingEntity) {
            CharacterTemplate cT = plugin.getCharacterManager().getCharacter((LivingEntity) e);
            if (cT.hasEffect("MageFire")) {
                applyMageFireStack((LivingEntity) event.getEntity(), cT);
                return;
            }
        } else {
            return;
        }
    }

    private void applyMageFireStack(LivingEntity target, CharacterTemplate applier) {
        long duration = Long.valueOf(applier.hasEffect("Drake") ? SkillConfigManager.getRaw(this, "drake-duration", "30000").replaceAll("[^0-9]", "") : SkillConfigManager.getRaw(this, "duration", "15000").replaceAll("[^0-9]", ""));
        CharacterTemplate cT = plugin.getCharacterManager().getCharacter(target);
        int stacks = 0;
        if (cT.hasEffect("MageFireDebuff")) {
            MageFireDebuffEffect effect = (MageFireDebuffEffect) cT.getEffect("MageFireDebuff");
            stacks = effect.getStacks();
            cT.removeEffect(effect);
        }
        stacks++;
        int maxStacks = Integer.valueOf(SkillConfigManager.getRaw(this, "max-stacks", "10").replaceAll("[^0-9]", ""));
        if (stacks > maxStacks) {
            stacks = maxStacks;
        }
        cT.addEffect(new MageFireDebuffEffect(this, this.plugin, duration, stacks));        
    }
    
    public class MageFireDebuffEffect extends ExpirableEffect {

        private int stacks;
        
        public MageFireDebuffEffect(Skill skill, Heroes plugin, long duration, int stacks) {
            super(skill, plugin, "MageFireDebuff", duration);
            this.stacks = stacks;
        }
        
        public int getStacks() {
            return stacks;
        }
        
    }
}
