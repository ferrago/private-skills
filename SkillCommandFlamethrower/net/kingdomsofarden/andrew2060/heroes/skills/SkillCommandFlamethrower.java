package net.kingdomsofarden.andrew2060.heroes.skills;


import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.api.SkillResult;
import com.herocraftonline.heroes.characters.Hero;
import com.herocraftonline.heroes.characters.effects.ExpirableEffect;
import com.herocraftonline.heroes.characters.effects.common.CombustEffect;
import com.herocraftonline.heroes.characters.skill.ActiveSkill;
import com.herocraftonline.heroes.characters.skill.Skill;
import com.herocraftonline.heroes.characters.skill.SkillConfigManager;
import com.herocraftonline.heroes.characters.skill.SkillSetting;
import net.kingdomsofarden.andrew2060.api.entity.EntityUtil;
import net.kingdomsofarden.andrew2060.heroes.skills.turretModules.TurretEffect;
import net.kingdomsofarden.andrew2060.heroes.skills.turretModules.TurretFireWrapper;
import net.minecraft.server.v1_8_R1.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.*;
import org.bukkit.entity.Entity;
import org.bukkit.event.entity.EntityCombustByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.util.Vector;

import java.util.List;

public class SkillCommandFlamethrower extends ActiveSkill {

    public SkillCommandFlamethrower(Heroes plugin) {
        super(plugin, "CommandFlamethrower");
        setDescription("Command: Flamethrower: Puts all active turrets in flamethrower state - Turrets will shoot a stream" +
                " of fireballs for three seconds at the closest target (prioritizes players), then deactivate for two seconds");
        setUsage("/skill commandflamethrower");
        setArgumentRange(0,0);
        setIdentifiers("skill commandflamethrower", "skill cflamethrower", "skill flamethrower");
        plugin.getServer().getScheduler().runTask(plugin, new Runnable() {
            @Override
            public void run() {
                EntityUtil.registerCustomEntity(SkillTurretFireball.class, "SkillFireballTurret", 13, false);
            }
        });
    }

    @Override
    public SkillResult use(Hero h, String[] arg1) {
        if (h.hasEffect("TurretEffectCooldown")) {
            h.getPlayer().sendMessage("You must wait 10 seconds between using different command skills!");
            return SkillResult.NORMAL;
        }
        broadcast(h.getPlayer().getLocation(), "§7[§2Skill§7] $1 activated flamethrower turret", new Object[] {h.getName()});
        h.addEffect(new ExpirableEffect(this,this.plugin,"TurretEffectCooldown",10000));
        TurretEffect tE;
        if (!h.hasEffect("TurretEffect")) {
            tE = new TurretEffect(plugin, this);
            h.addEffect(tE);
        } else {
            tE = (TurretEffect) h.getEffect("TurretEffect");
        }
        FlameThrowerTurret fireFunc = new FlameThrowerTurret(this);
        tE.setFireFunctionWrapper(fireFunc);
        return SkillResult.NORMAL;
    }
    private class FlameThrowerTurret extends TurretFireWrapper {

        private SkillCommandFlamethrower skill;
        private Long nextDeactivation;
        private Long nextActivation;
        private LivingEntity foundLE;

        public FlameThrowerTurret(SkillCommandFlamethrower skill) {
            this.skill = skill;
            this.nextActivation = null;
            this.nextDeactivation = null;
            this.foundLE = null;
        }

        @Override
        public void fire(final Hero h, Location loc, double range, List<LivingEntity> validTargets) {
            if (this.nextActivation == null) {
                this.nextActivation = loc.getWorld().getFullTime();
                this.nextDeactivation = this.nextActivation + 60;
            }
            long serverTicks = loc.getWorld().getFullTime();
            if (serverTicks == this.nextDeactivation.longValue()) {
                this.nextActivation += 100;
                this.foundLE = null;
                return;
            }
            if (serverTicks >= this.nextDeactivation.longValue() && serverTicks < this.nextActivation.longValue()) {
                return;
            } else if (serverTicks == this.nextActivation.longValue()) {
                this.nextDeactivation += 100;
            }

            if (validTargets == null || validTargets.isEmpty()) {
                return;
            }
            final Location entitySpawnSource = loc.clone().add(0, 1, 0);
            if (foundLE == null) {
                boolean closestIsPlayer = false;
                LivingEntity closest = null;
                double closestDistanceSquared = Double.MAX_VALUE - 1;
                for (LivingEntity lE : validTargets) {
                    if (!(lE instanceof Player)) {
                        if (closestIsPlayer) {
                            continue;
                        } else {
                            double distanceSquared = lE.getLocation().distanceSquared(entitySpawnSource);
                            if (distanceSquared < closestDistanceSquared) {
                                closestDistanceSquared = distanceSquared;
                                closest = lE;
                            }
                        }
                    } else {
                        double distanceSquared = lE.getLocation().distanceSquared(entitySpawnSource);
                        if (distanceSquared < closestDistanceSquared) {
                            closestDistanceSquared = distanceSquared;
                            closest = lE;
                            closestIsPlayer = true;
                        }
                    }
                }
                this.foundLE = closest;
            }
            final LivingEntity found = this.foundLE;
            for (int i = 0; i < 2; i++) {
                plugin.getServer().getScheduler().runTaskLater(plugin, new Runnable() {

                    @Override
                    public void run() {
                        Vector shootVector = found.getLocation().subtract(entitySpawnSource).toVector().setY(0).normalize();
                        Location entitySpawn = entitySpawnSource.clone().add(shootVector);
                        @SuppressWarnings("deprecation")
                        Vector movement = found.getVelocity();
                        CraftWorld cWorld = (CraftWorld) entitySpawn.getWorld();
                        movement.setY(movement.getY() * 0.5); // Reduce y changes ... i.e. for jumping
                        Location destSource = found.getLocation().add(0, 0.5, 0);
                        double angle = movement.angle(destSource.subtract(entitySpawn).toVector());
                        double length = movement.length();
                        double sinFAngle = length * MathHelper.sin((float) angle) / 1.90;
                        double fAngle = Math.asin(sinFAngle);
                        double crossAngle = Math.PI - angle - fAngle;
                        double playerSpeed = found.getLocation().subtract(entitySpawn).toVector().length();
                        double time = playerSpeed * sinFAngle / (Math.sin(crossAngle) * length);
                        Location dest = destSource.clone().add(movement.multiply(time));
                        Vector v = dest.subtract(entitySpawn).toVector();
                        v = v.normalize().multiply(1.90);
                        SkillTurretFireball fireball = new SkillTurretFireball(cWorld.getHandle(),
                                ((CraftPlayer) h.getPlayer()).getHandle(), v.getX(), v.getY(), v.getZ(), h, skill);
                        fireball.projectileSource = h.getPlayer();
                        fireball.setPosition(entitySpawn.getX(), entitySpawn.getY(), entitySpawn.getZ());
                        cWorld.getHandle().addEntity(fireball);
                        fireball.getBukkitEntity().setVelocity(v.normalize().multiply(1.90));
                    }
                }, 10 * i);
            }
        }
    }


    @Override
    public String getDescription(Hero h) {
        return getDescription();
    }

    public static class SkillTurretFireball extends EntitySmallFireball {

        private Hero hero;
        private Skill skill;

        //Load due to unload/saved entity
        public SkillTurretFireball(World world) {
            super(world);
            this.die();
        }

        public SkillTurretFireball(World world, EntityPlayer player, double x, double y, double z, Hero hero, Skill skill) {
            super(world,player,x,y,z);
            this.hero = hero;
            this.skill = skill;
        }

        @Override
        public void setDirection(double d0, double d1, double d2) {
            double d3 = (double) MathHelper.sqrt(d0 * d0 + d1 * d1 + d2 * d2);

            this.dirX = d0 / d3 * 0.1D;
            this.dirY = d1 / d3 * 0.1D;
            this.dirZ = d2 / d3 * 0.1D;
        }

        protected void a(MovingObjectPosition movingobjectposition) {
            try {
                if (!this.world.isStatic) {
                    if (movingobjectposition.entity != null) {
                        Entity target = movingobjectposition.entity.getBukkitEntity();
                        if (!(target instanceof LivingEntity)) {
                            return;
                        }
                        if (Skill.damageCheck(hero.getPlayer(), (LivingEntity) target)) {
                            int fireLength = (int) Math.ceil(SkillConfigManager.getUseSetting(hero, skill, "fire-ticks", 100, false)/20);
                            EntityCombustByEntityEvent event = new EntityCombustByEntityEvent(hero.getPlayer(), target, fireLength);
                            Heroes.getInstance().getServer().getPluginManager().callEvent(event);
                            if (!event.isCancelled()) {
                                target.setFireTicks(event.getDuration() * 20);
                                Heroes.getInstance().getCharacterManager().getCharacter((LivingEntity) target).addEffect(new CombustEffect(skill, (Player) hero.getPlayer()));
                                skill.addSpellTarget(target, hero);
                                double damage = SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE, 10, false);
                                damage += (SkillConfigManager.getUseSetting(hero, skill, SkillSetting.DAMAGE_INCREASE, 0.0, false) * hero.getSkillLevel(skill));
                                Skill.damageEntity((LivingEntity) target, hero.getPlayer(), damage, EntityDamageEvent.DamageCause.MAGIC, false);
                            }
                        }
                    }

                    this.die();
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
                this.die();
            }
        }
    }

}
